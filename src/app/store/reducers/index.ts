import {
  ActionReducerMap,
  createSelector,
  createFeatureSelector,
  ActionReducer,
  MetaReducer,
} from '@ngrx/store';

import * as fromTasks from './my-tasks';

export interface State {
  tasks: fromTasks.State;
}

export const reducers: ActionReducerMap<State> = {
  tasks: fromTasks.reducer
};


export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return function (state: State, action: any): State {
    return reducer(state, action);
  };
}

export const metaReducers: MetaReducer<State>[] = [logger];


export const getTaskState = createFeatureSelector<fromTasks.State>('tasks');


export const getTasks = createSelector(
  getTaskState,
  fromTasks.getTasks,
);

export const getTotal = createSelector(
  getTaskState,
  fromTasks.getTotal,
);

export const getError = createSelector(
  getTaskState,
  fromTasks.getError
);

export const getFetch = createSelector(
  getTaskState,
  fromTasks.getFetch
);
