import * as taskAction from '../actions/action';
import {Task} from '../../models/task';


export interface State {
  isFetching: boolean;
  hasError: boolean;
  tasks: Task[];
  totalPages: number;
  elementsPerPage: number;
  totalItems: number;
}

export const initialState: State = {
  isFetching: false,
  hasError: false,
  tasks: [],
  totalPages: 0,
  elementsPerPage: null,
  totalItems: 0
};

export function reducer(state = initialState, action: taskAction.Action) {
  switch (action.type) {
    case taskAction.ADD_ONE: {
      const newTask: Task = action.payload;
      if (state.tasks.length < state.elementsPerPage) {
        return {
          ...state,
          isFetching: false,
          tasks: [...state.tasks, newTask],
          totalItems: state.totalItems + 1,
          totalPages: state.totalItems % state.elementsPerPage === 0 ? state.totalPages + 1 : state.totalPages
        };
      }
      return {
        ...state,
        isFetching: false,
        totalItems: state.totalItems + 1,
        totalPages: state.totalItems % state.elementsPerPage === 0 ? state.totalPages + 1 : state.totalPages
      };
    }

    case taskAction.ADD_TOTAL: {
      const newTotal: number = action.payload;

      return {
        ...state,
        totalPages: newTotal
      };
    }

    case taskAction.ADD_ELEMENTS_PER_PAGE: {
      const elements: number = action.payload;

      return {
        ...state,
        elementsPerPage: elements
      };
    }

    case taskAction.ADD_ALL: {
      const newTasks: Task[] = action.payload;

      return {
        ...state,
        isFetching: false,
        tasks: [...state.tasks, ...newTasks],
        totalItems: state.totalItems + newTasks.length
      };
    }

    case taskAction.DELETE_ONE: {
      const idInp = action.payload;
      return {
        ...state,
        tasks: state.tasks.filter(({id}) => id !== idInp),
        totalItems: state.totalItems - 1,
        totalPages: (state.totalItems - 1) % state.elementsPerPage === 0 ? state.totalPages - 1 : state.totalPages
      }
      ;
    }

    case taskAction.DELETE_ALL: {
      return {
        ...state,
        tasks: [],
        totalItems: 0
      };
    }

    case taskAction.FETCH_ITEM: {
      return {
        ...state,
        isFetching: true,
        hasError: false,
      };
    }
    case taskAction.ERROR_ITEM: {
      return {
        ...state,
        isFetching: false,
        hasError: true,
      };
    }

    default:
      return state;
  }
}

export const getTasks = (state: State) => state.tasks;
export const getError = (state: State) => state.hasError;
export const getFetch = (state: State) => state.isFetching;
export const getTotal = (state: State) => state.totalPages;
