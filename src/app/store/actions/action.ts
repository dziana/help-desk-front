import { Action } from '@ngrx/store';
import {Task} from '../../models/task';

export const ADD_ONE = '[Tasks] Add One';
export const ADD_ALL = '[Tasks] Add All';
export const DELETE_ONE = '[Tasks] Delete One';
export const DELETE_ALL = '[Tasks] Delete All';
export const FETCH_ITEM = '[Tasks] Fetch';
export const ERROR_ITEM = '[Tasks] Error';
export const ADD_TOTAL = '[Tasks] Add Total';
export const ADD_ELEMENTS_PER_PAGE = '[Tasks] Add Elements per page';

export class AddOne implements Action {
  readonly type = ADD_ONE;
  constructor(public payload: Task) { }
}

export class AddTotal implements Action {
  readonly type = ADD_TOTAL;
  constructor(public payload: number) { }
}

export class AddElementsPerPage implements Action {
  readonly type = ADD_ELEMENTS_PER_PAGE;
  constructor(public payload: number) { }
}

export class AddAll implements Action {
  readonly type = ADD_ALL;
  constructor(public payload: Task[]) { }
}

export class DeleteOne implements Action {
  readonly type = DELETE_ONE;
  constructor(public payload: number) { }
}

export class DeleteAll implements Action {
  readonly type = DELETE_ALL;
  constructor() { }
}

export class Fetch implements Action {
  readonly type = FETCH_ITEM;
  constructor(public payload) {
  }
}

export class Error implements Action {
  readonly type = ERROR_ITEM;
  constructor(public payload: string) {
  }
}
export type Action = AddOne | DeleteOne | DeleteAll | Fetch | Error | AddAll | AddTotal | AddElementsPerPage;


