import {Injectable} from '@angular/core';
import * as taskActions from '../actions/action';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import {AddTotal, Fetch} from '../actions/action';
import {TasksService} from '../../services/tasks.service';
import {Store} from '@ngrx/store';
import * as fromRoot from '../reducers';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class LoadService {

  constructor(
    private actions$: Actions,
    private tasksService: TasksService,
    private store: Store<fromRoot.State>
  ) {
  }

  @Effect()
  loadValue$: Observable<any> = this.actions$.pipe(
    ofType(taskActions.FETCH_ITEM),
    map((action: Fetch) => action.payload),
    switchMap((payload) => {
      return this.tasksService.getPersonalTasks(payload)
        .pipe(
          map((value) => {
            this.store.dispatch(new AddTotal(value['totalPages']));
            return new taskActions.AddAll(value['content']);
          }),

          catchError(error => of(new taskActions.Error(error)))
        );
    })
  );
}

