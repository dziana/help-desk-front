import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../services/user.service';
import {User} from '../../../models/user';
import {Subdivision} from '../../../models/subdivision';
import {Branch} from '../../../models/branch';

@Component({
  selector: 'app-update-user-form',
  templateUrl: './update-user-form.component.html',
  styleUrls: ['./update-user-form.component.css']
})
export class UpdateUserFormComponent implements OnInit {
  updateUserForm: FormGroup;
  loading = false;
  submitted = false;
  error = false;

  @Input() user: User;
  @Input() visible: boolean;
  @Input() subdivisions: Subdivision[];
  @Input() branches: Branch[];
  @Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() userChange: EventEmitter<any> = new EventEmitter<any>();

  constructor(private formBuilder: FormBuilder,
              private userService: UserService) {
  }

  ngOnInit() {
    this.updateUserForm = this.formBuilder.group({
      name: [this.user.name, Validators.required],
      family: [this.user.family, Validators.required],
      email: [this.user.email, [Validators.required, Validators.email]],
      branchId: [this.user.branchId ? this.user.branchId.id : '', Validators.required],
      subdivisionId: [this.user.subdivisionId ? this.user.subdivisionId.id : '', Validators.required],
      password: ['']
    });
  }

  get f() {
    return this.updateUserForm.controls;
  }

  close() {
    this.visible = false;
    this.visibleChange.emit(this.visible);
  }

  onSubmit() {
    this.submitted = true;
    if (this.updateUserForm.invalid) {
      return;
    }
    this.loading = true;

    this.userService.updateUser(this.user.id, this.updateUserForm.value).subscribe(
      () => {
        this.error = false;
        this.userChange.emit(this.updateUserForm.value);
        this.loading = false;
        this.close();
      },
      () => {
        this.error = true;
        this.loading = false;
      }
    );
  }

}
