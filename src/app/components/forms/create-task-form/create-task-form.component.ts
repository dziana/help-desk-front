import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TasksService} from '../../../services/tasks.service';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../store/reducers';
import {AddOne} from '../../../store/actions/action';
import {ListenersService} from '../../../services/listeners.service';
import {Listener} from '../../../models/listener';
import {ToastrService} from 'ngx-toastr';


@Component({
  selector: 'app-create-task-form',
  templateUrl: './create-task-form.component.html',
  styleUrls: ['./create-task-form.component.css']
})
export class CreateTaskFormComponent implements OnInit {
  createTaskForm: FormGroup;
  formOpened = false;
  loading = false;
  submitted = false;
  listeners: Listener[];
  error = false;

  constructor(private formBuilder: FormBuilder,
              private tasksService: TasksService,
              private listenerService: ListenersService,
              private store: Store<fromRoot.State>,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.getListeners();
    this.createTaskForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: [''],
      listenerId: ['', Validators.required]
    });
  }

  get f() {
    return this.createTaskForm.controls;
  }

  getListeners() {
    this.listenerService.getAllListeners().subscribe(res => {
      this.listeners = res;
    }, () => {
      this.error = true;
    });
  }

  saveTask(task) {
    this.store.dispatch(new AddOne(
      task
    ));
  }


  onSubmit() {
    this.submitted = true;
    if (this.createTaskForm.invalid) {
      return;
    }
    this.loading = true;

    this.tasksService.createTask(this.createTaskForm.value).subscribe(res => {
        this.error = false;
        this.submitted = false;
        this.loading = false;
        this.saveTask(res);
        this.createTaskForm.reset();
      }, () => {
        this.error = true;
        this.loading = false;
        this.createTaskForm.reset();
      }
    );
  }

}
