import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Branch} from '../../../models/branch';
import {BranchesService} from '../../../services/branches.service';

@Component({
  selector: 'app-update-branch-form',
  templateUrl: './update-branch-form.component.html',
  styleUrls: ['./update-branch-form.component.css']
})
export class UpdateBranchFormComponent implements OnInit {
  updateBranchForm: FormGroup;
  loading = false;
  submitted = false;
  error = false;

  @Input() branch: Branch;
  @Input() visible: boolean;
  @Input() type: string;
  @Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() branchChange: EventEmitter<any> = new EventEmitter<any>();

  constructor(private formBuilder: FormBuilder,
              private branchesService: BranchesService) {
  }

  ngOnInit() {
    this.updateBranchForm = this.formBuilder.group({
      name: [this.type === 'update' ? this.branch.name : '', Validators.required],
      address: [this.type === 'update' ? this.branch.address : '', Validators.required]
    });
  }

  get f() {
    return this.updateBranchForm.controls;
  }

  close() {
    this.visible = false;
    this.visibleChange.emit(this.visible);
  }

  onSubmit() {
    this.submitted = true;
    if (this.updateBranchForm.invalid) {
      return;
    }
    this.loading = true;

    switch (this.type) {
      case 'update': {
        this.branchesService.updateBranch(this.branch.id, this.updateBranchForm.value).subscribe(
          () => {
            this.error = false;
            this.branchChange.emit(this.updateBranchForm.value);
            this.loading = false;
            this.close();
          },
          () => {
            this.error = true;
            this.loading = false;
          }
        );
        break;
      }
      case 'create': {
        this.branchesService.saveBranch(this.updateBranchForm.value).subscribe(
          () => {
            this.error = false;
            this.loading = false;
            this.branchChange.emit(this.updateBranchForm.value);
            this.close();
          },
          () => {
            this.error = true;
            this.loading = false;
          }
        );
        break;
      }
      default: {
      }
    }

  }
}
