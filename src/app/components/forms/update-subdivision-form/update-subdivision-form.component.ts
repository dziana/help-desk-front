import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subdivision} from '../../../models/subdivision';
import {SubdivisionService} from '../../../services/subdivisions.service';

@Component({
  selector: 'app-update-subdivision-form',
  templateUrl: './update-subdivision-form.component.html',
  styleUrls: ['./update-subdivision-form.component.css']
})
export class UpdateSubdivisionFormComponent implements OnInit {
  updateSubdivisionForm: FormGroup;
  loading = false;
  submitted = false;
  error = false;

  @Input() subdivision: Subdivision;
  @Input() visible: boolean;
  @Input() type: string;
  @Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() subdivisionChange: EventEmitter<any> = new EventEmitter<any>();

  constructor(private formBuilder: FormBuilder,
              private subdivisionsService: SubdivisionService) {
  }

  ngOnInit() {
    this.updateSubdivisionForm = this.formBuilder.group({
      name: [this.type === 'update' ? this.subdivision.name : '', Validators.required]
    });
  }

  get f() {
    return this.updateSubdivisionForm.controls;
  }

  close() {
    this.visible = false;
    this.visibleChange.emit(this.visible);
  }

  onSubmit() {
    this.submitted = true;
    if (this.updateSubdivisionForm.invalid) {
      return;
    }
    this.loading = true;

    switch (this.type) {
      case 'update': {
        this.subdivisionsService.updateSubdivision(this.subdivision.id, this.updateSubdivisionForm.value).subscribe(
          () => {
            this.error = false;
            this.loading = false;
            this.subdivisionChange.emit(this.updateSubdivisionForm.value);
            this.close();
          },
          () => {
            this.error = true;
            this.loading = false;
          }
        );
        break;
      }
      case 'create': {
        this.subdivisionsService.saveSubdivision(this.updateSubdivisionForm.value).subscribe(
          () => {
            this.error = false;
            this.loading = false;
            this.subdivisionChange.emit(this.updateSubdivisionForm.value);
            this.close();

          },
          () => {
            this.error = true;
            this.loading = false;
          }
        );
        break;
      }
      default: {
        this.error = true;
      }
    }
  }
}
