import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Listener} from '../../../models/listener';
import {ListenersService} from '../../../services/listeners.service';
import {User} from '../../../models/user';
import {Subdivision} from '../../../models/subdivision';

@Component({
  selector: 'app-update-listener-form',
  templateUrl: './update-listener-form.component.html',
  styleUrls: ['./update-listener-form.component.css']
})
export class UpdateListenerFormComponent implements OnInit {
  updateListenerForm: FormGroup;
  loading = false;
  submitted = false;
  error = false;

  @Input() listener: Listener;
  @Input() visible: boolean;
  @Input() operators: User[];
  @Input() subdivisions: Subdivision[];
  @Input() type: string;
  @Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() listenerChange: EventEmitter<any> = new EventEmitter<any>();

  constructor(private formBuilder: FormBuilder,
              private listenersService: ListenersService) {
  }

  ngOnInit() {
    this.updateListenerForm = this.formBuilder.group({
      subdivisionId: [this.type === 'update' ? this.listener.subdivisionId.id : '', Validators.required],
      systemUserId: [this.type === 'update' ? this.listener.systemUserId.id : '', Validators.required]
    });
  }

  get f() {
    return this.updateListenerForm.controls;
  }

  close() {
    this.visible = false;
    this.visibleChange.emit(this.visible);
  }

  onSubmit() {
    this.submitted = true;
    if (this.updateListenerForm.invalid) {
      return;
    }
    this.loading = true;


    switch (this.type) {
      case 'update': {
        this.listenersService.updateListener(this.listener.id, this.updateListenerForm.value).subscribe(
          () => {
            this.error = false;
            this.loading = false;
            this.listenerChange.emit(this.updateListenerForm.value);
            this.close();
          },
          () => {
            this.error = true;
            this.loading = false;
          }
        );
        break;
      }
      case 'create': {
        this.listenersService.saveListener(this.updateListenerForm.value).subscribe(
          () => {
            this.error = false;
            this.loading = false;
            this.listenerChange.emit(this.updateListenerForm.value);
            this.close();

          },
          () => {
            this.error = true;
            this.loading = false;
          }
        );
        break;
      }
      default: {
        this.error = true;
      }
    }
  }

}
