import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {first} from 'rxjs/operators';
import {AuthenticationService} from '../../../services/authentification.service';
import {Role} from '../../../models/role';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  error = false;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private authenticationService: AuthenticationService) {
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(this.selectPathToNavigate());
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.loginForm.value)
      .pipe(first())
      .subscribe(
        () => {
          this.error = false;
          this.loading = false;
          this.router.navigate(this.selectPathToNavigate());
        },
        () => {
          this.error = true;
          this.loading = false;
        });
  }

  selectPathToNavigate() {
    if (this.authenticationService.hasRole(Role.User)) {
      return ['/mytasks'];
    }
    if (this.authenticationService.hasRole(Role.Operator)) {
      return ['/assigntasks'];
    }
    if (this.authenticationService.hasRole(Role.Executor)) {
      return ['/performtasks'];
    }
    if (this.authenticationService.hasRole(Role.Admin)) {
      return ['/admin'];
    }
    return [''];
  }
}
