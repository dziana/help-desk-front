import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../../services/authentification.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  visibleContent: boolean;

  constructor(private authenticationService: AuthenticationService,
              private router: Router
  ) {
  }

  ngOnInit() {
    this.visibleContent = false;
  }

  showContent(value: boolean) {
    this.visibleContent = value;
  }

  logout() {
    this.visibleContent = false;
    this.authenticationService.logout();
    this.router.navigate(['/auth/login']);
  }

}
