import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavigationEnd, Router, RouterEvent} from '@angular/router';

@Component({
  selector: 'app-change-language',
  templateUrl: './change-language.component.html',
  styleUrls: ['./change-language.component.css']
})
export class ChangeLanguageComponent implements OnInit, OnDestroy {

  visibleContent: boolean;
  subscription;
  currentUrl;

  languages = [
    {code: 'ru', label: 'Русский'},
    {code: 'en', label: 'English'},
  ];

  constructor(private router: Router
  ) {
  }

  ngOnInit() {
    this.visibleContent = false;
    this.subscription = this.router.events
      .subscribe( (event: RouterEvent) => {
        if (event instanceof NavigationEnd) {
          this.currentUrl = this.router.url;
        }
      });
  }

  showContent(value: boolean) {
    this.visibleContent = value;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
