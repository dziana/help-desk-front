import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-change-theme',
  templateUrl: './change-theme.component.html',
  styleUrls: ['./change-theme.component.css']
})
export class ChangeThemeComponent implements OnInit {

  visibleContent: boolean;

  constructor(
  ) { }

  ngOnInit() {
    this.visibleContent = false;
  }

  showContent(value: boolean) {
    this.visibleContent = value;
  }

  changeTheme(color) {
    document.documentElement.style.setProperty('--main-hue', 'var(--' + color + '-hue)');
  }

}
