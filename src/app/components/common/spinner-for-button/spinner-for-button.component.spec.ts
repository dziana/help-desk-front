import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpinnerForButtonComponent } from './spinner-for-button.component';

describe('SpinnerForButtonComponent', () => {
  let component: SpinnerForButtonComponent;
  let fixture: ComponentFixture<SpinnerForButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpinnerForButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpinnerForButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
