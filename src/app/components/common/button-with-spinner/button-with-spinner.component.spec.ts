import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonWithSpinnerComponent } from './button-with-spinner.component';
import {SpinnerForButtonComponent} from '../spinner-for-button/spinner-for-button.component';

describe('ButtonWithSpinnerComponent', () => {
  let component: ButtonWithSpinnerComponent;
  let fixture: ComponentFixture<ButtonWithSpinnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonWithSpinnerComponent, SpinnerForButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonWithSpinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
