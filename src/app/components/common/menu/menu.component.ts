import {Component, HostBinding, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../../services/authentification.service';
import {Token} from '../../../models/token';
import {Role} from '../../../models/role';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
})
export class MenuComponent implements OnInit {
  currentUser: Token;
  assignOpen = false;
  adminOpen = false;

  get role() {
    return Role;
  }

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit() {
  }

  hasRole(role) {
    return this.authenticationService.hasRole(role);
  }
}
