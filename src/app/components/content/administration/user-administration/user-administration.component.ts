import {Component, OnInit} from '@angular/core';
import {User} from '../../../../models/user';
import {UserService} from '../../../../services/user.service';
import {Subdivision} from '../../../../models/subdivision';
import {Branch} from '../../../../models/branch';
import {BranchesService} from '../../../../services/branches.service';
import {SubdivisionService} from '../../../../services/subdivisions.service';
import {UserPage} from '../../../../models/userPage';
import {forkJoin} from 'rxjs';
import {ExcelService} from '../../../../services/excel.service';

@Component({
  selector: 'app-user-administration',
  templateUrl: './user-administration.component.html',
  styleUrls: ['./user-administration.component.css'],
})
export class UserAdministrationComponent implements OnInit {

  users: User[];
  selectedUser: User;
  openDialog = false;
  subdivisions: Subdivision[];
  branches: Branch[];
  error = false;
  isFetching = false;
  sizePerPage = 10;
  page = 1;
  totalPages = 1;

  constructor(private userService: UserService,
              private branchesService: BranchesService,
              private subdivisionsService: SubdivisionService) {
  }

  ngOnInit() {
    this.getDataFirstTime();
  }

  getDataFirstTime() {
    this.isFetching = true;
    const parameters = {
      page: this.page - 1,
      size: this.sizePerPage
    };
    forkJoin(
      this.userService.getAllUsers(parameters),
      this.subdivisionsService.getAllSubdivisions(),
      this.branchesService.getAllBranches()
    ).subscribe(
      res => {
        this.error = false;
        this.isFetching = false;
        this.users = res[0].content;
        this.totalPages = res[0].totalPages;
        this.subdivisions = res[1];
        this.branches = res[2];
      },
      err => {
        this.error = true;
        this.isFetching = false;
      }
    );
  }

  getData() {
    this.isFetching = true;
    const parameters = {
      page: this.page - 1,
      size: this.sizePerPage
    };
    this.userService.getAllUsers(parameters).subscribe((res: UserPage) => {
        this.error = false;
        this.isFetching = false;
        this.users = res.content;
        this.totalPages = res.totalPages;
      },
      error => {
        this.error = true;
        this.isFetching = false;
      }
    );
  }

  selectUser(user) {
    this.openDialog = true;
    this.selectedUser = user;
  }

  userChange(form) {
    this.getData();
  }

  changePage(newPage) {
    this.page = newPage;
    this.getData();
  }

  deleteUser(id) {
    this.isFetching = true;
    this.userService.deleteUser(id).subscribe(
      res => {
        this.isFetching = false;
        this.users = this.users.filter(e => {
          return e.id !== id;
        });
      },
      err => {
        this.isFetching = false;
      }
    );
  }
}
