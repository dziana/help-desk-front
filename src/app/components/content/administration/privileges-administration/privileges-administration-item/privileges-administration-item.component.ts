import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../../../models/user';
import {UserService} from '../../../../../services/user.service';
import {Privilege} from '../../../../../models/privilege';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';


@Component({
  selector: 'app-privileges-administration-item',
  templateUrl: './privileges-administration-item.component.html',
  styleUrls: ['./privileges-administration-item.component.css']
})
export class PrivilegesAdministrationItemComponent implements OnInit {

  @Input() user: User;
  @Input() privileges: Privilege[];

  form: FormGroup;

  constructor(private userService: UserService,
              private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      privs: new FormArray([])
    });

    this.addCheckboxes();
    console.log(this.form.controls.privs);
  }

  private addCheckboxes() {
    this.privileges.map((o) => {
      const found = this.user.privilege.some(r => o.id === r.id);
      const control = new FormControl(found);
      (this.form.controls.privs as FormArray).push(control);
    });
  }

  submit() {
    let privilegeList = [];
    this.form.get('privs').value.map((o, i) => {
      if (o) {
        privilegeList = [...privilegeList, this.privileges[i].id];
      }
    });
    this.userService.updateUserPrivileges(this.user.id, privilegeList).subscribe(() => {
    });
  }
}
