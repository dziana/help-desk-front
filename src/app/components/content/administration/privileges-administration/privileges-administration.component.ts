import {Component, OnInit} from '@angular/core';
import {Privilege} from '../../../../models/privilege';
import {User} from '../../../../models/user';
import {UserService} from '../../../../services/user.service';
import {PrivilegeService} from '../../../../services/privilege.service';
import {UserPage} from '../../../../models/userPage';
import {forkJoin} from 'rxjs';
import {TranslationPipe} from '../../../../pipes/translate.pipe';

@Component({
  selector: 'app-privileges-administration',
  templateUrl: './privileges-administration.component.html',
  styleUrls: ['./privileges-administration.component.css'],
  providers: [TranslationPipe]
})
export class PrivilegesAdministrationComponent implements OnInit {

  privileges: Privilege[];
  users: User[];
  error = false;
  isFetching = false;
  sizePerPage = 10;
  page = 1;
  totalPages = 1;


  constructor(private privilegeService: PrivilegeService,
              private userService: UserService,
              private translate: TranslationPipe) {
  }

  ngOnInit() {
    this.getDataFirst();
  }

  getData() {
    this.isFetching = true;
    const parameters = {
      page: this.page - 1,
      size: this.sizePerPage
    };
    this.userService.getAllUsers(parameters).subscribe((res: UserPage) => {
        this.error = false;
        this.isFetching = false;
        this.users = res.content;
        this.totalPages = res.totalPages;
      },
      error => {
        this.error = true;
        this.isFetching = false;
      }
    );
  }

  changePage(newPage) {
    this.page = newPage;
    this.getData();
  }

  translatePriviliges(privileges) {
    privileges.forEach(e => {
      switch (e.name) {
        case 'ROLE_USER': {
          e['translate_name'] = this.translate.transform('user');
          break;
        }
        case 'ROLE_OPERATOR': {
          e['translate_name'] = this.translate.transform('operator');
          break;
        }
        case 'ROLE_EXECUTOR': {
          e['translate_name'] = this.translate.transform('executor');
          break;
        }
        case 'ROLE_ADMIN': {
          e['translate_name'] = this.translate.transform('admin');
          break;
        }
        default: {
          e['translate_name'] = e.name;
        }
      }
    });
    return privileges;
  }

  getDataFirst() {
    this.isFetching = true;
    const parameters = {
      page: this.page - 1,
      size: this.sizePerPage
    };
    forkJoin(this.userService.getAllUsers(parameters), this.privilegeService.getAllPriviliges()).subscribe(res => {
        this.error = false;
        this.isFetching = false;
        this.users = res[0].content;
        this.totalPages = res[0].totalPages;
        this.privileges = this.translatePriviliges(res[1]);
        this.privileges = res[1];

      },
      error => {
        this.error = true;
        this.isFetching = false;
      }
    );
  }
}
