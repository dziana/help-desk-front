import {Component, OnInit} from '@angular/core';
import {Branch} from '../../../../models/branch';
import {BranchesService} from '../../../../services/branches.service';

@Component({
  selector: 'app-branches-administration',
  templateUrl: './branches-administration.component.html',
  styleUrls: ['./branches-administration.component.css']
})
export class BranchesAdministrationComponent implements OnInit {

  branches: Branch[];
  hasError;
  isFetching = false;
  selectedBranch: Branch;
  openDialog = false;
  dialogType = 'update';
  constructor(private branchService: BranchesService) {
  }

  ngOnInit() {
    this.getBranches();
  }

  getBranches() {
    this.isFetching = true;
    this.branchService.getAllBranches().subscribe(res => {
      this.hasError = '';
      this.isFetching = false;
      this.branches = res;
    }, err => {
      this.isFetching = false;
      this.hasError = err;
    });
  }

  selectBranch(branch) {
    this.dialogType = 'update';
    this.openDialog = true;
    this.selectedBranch = branch;
  }

  createBranch() {
    this.dialogType = 'create';
    this.openDialog = true;
  }

  changeBranch() {
    this.getBranches();
  }

  deleteBranch(id) {
    this.isFetching = true;
    this.branchService.deleteBranch(id).subscribe(
      () => {
        this.hasError = '';
        this.isFetching = false;
        this.branches = this.branches.filter(e => {
          return e.id !== id;
        });
      },
      () => {
        this.hasError = '';
        this.isFetching = false;
      }
    );
  }
}

