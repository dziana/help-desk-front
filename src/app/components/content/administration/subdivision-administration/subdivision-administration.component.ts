import {Component, OnInit} from '@angular/core';
import {SubdivisionService} from '../../../../services/subdivisions.service';
import {Subdivision} from '../../../../models/subdivision';

@Component({
  selector: 'app-subdivision-administration',
  templateUrl: './subdivision-administration.component.html',
  styleUrls: ['./subdivision-administration.component.css']
})
export class SubdivisionAdministrationComponent implements OnInit {

  subdivisions: Subdivision[];
  hasError;
  isFetching = false;
  selectedSubdivision: Subdivision;
  openDialog = false;
  dialogType: string;

  constructor(private subdivisionsService: SubdivisionService) {
  }

  ngOnInit() {
    this.getSubdivisions();
  }

  getSubdivisions() {
    this.isFetching = true;
    this.subdivisionsService.getAllSubdivisions().subscribe(res => {
      this.hasError = '';
      this.isFetching = false;
      this.subdivisions = res;
    }, err => {
      this.isFetching = false;
      this.hasError = err;
    });
  }

  selectSubdivision(subdivision) {
    this.dialogType = 'update';
    this.openDialog = true;
    this.selectedSubdivision = subdivision;
  }

  changeSubdivision() {
    this.getSubdivisions();
  }

  createSubdivision() {
    this.dialogType = 'create';
    this.openDialog = true;
  }

  deleteSubdivision(id) {
    this.isFetching = true;
    this.subdivisionsService.deleteSubdivision(id).subscribe(
      () => {
        this.hasError = '';
        this.isFetching = false;
        this.subdivisions = this.subdivisions.filter(e => {
          return e.id !== id;
        });
      },
      () => {
        this.hasError = '';
        this.isFetching = false;
      }
    );
  }
}

