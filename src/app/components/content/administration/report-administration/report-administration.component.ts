import {Component, OnInit} from '@angular/core';
import {ExcelService} from '../../../../services/excel.service';
import {forkJoin} from 'rxjs';
import {UserService} from '../../../../services/user.service';
import {SubdivisionService} from '../../../../services/subdivisions.service';
import {BranchesService} from '../../../../services/branches.service';
import {TasksService} from '../../../../services/tasks.service';
import {ListenersService} from '../../../../services/listeners.service';
import {TranslationPipe} from '../../../../pipes/translate.pipe';

@Component({
  selector: 'app-report-administration',
  templateUrl: './report-administration.component.html',
  styleUrls: ['./report-administration.component.css'],
  providers: [TranslationPipe]
})
export class ReportAdministrationComponent implements OnInit {
  loading = false;

  selectedResultItem;
  resultItemsList = [];

  constructor(private excelService: ExcelService,
              private usersService: UserService,
              private subdivisionService: SubdivisionService,
              private listenerService: ListenersService,
              private branchesService: BranchesService,
              private tasksService: TasksService,
              private translate: TranslationPipe) {
  }

  itemsList = [
    {
      id: 0,
      name: this.translate.transform('users')
    },
    {
      id: 1,
      name: this.translate.transform('subdivisions')
    },
    {
      id: 2,
      name: this.translate.transform('listeners')
    },
    {
      id: 3,
      name: this.translate.transform('branches')
    },
    {
      id: 4,
      name: this.translate.transform('tasks')
    }
  ];
  selectedItem = this.itemsList[0];

  ngOnInit() {
    console.log(this.translate.transform('users'));
  }

  download() {
    if (this.resultItemsList.length !== 0) {
      const ids = this.resultItemsList.map(({id}) => id);
      let observableBatch = [];
      let reportArray = [];
      if (ids.includes(0)) {
        observableBatch.push(this.usersService.getAllUsersForReport());
      }
      if (ids.includes(1)) {
        observableBatch.push(this.subdivisionService.getAllSubdivisions());
      }
      if (ids.includes(2)) {
        observableBatch.push(this.listenerService.getAllListenersForReport());
      }
      if (ids.includes(3)) {
        observableBatch.push(this.branchesService.getAllBranches());
      }
      if (ids.includes(4)) {
        observableBatch.push(this.tasksService.getAllTasksForReport());
      }
      this.loading = true;
      forkJoin(observableBatch).subscribe((res) => {
        this.loading = false;
        let counter = 0;
        for (const id of ids) {
          reportArray = [...reportArray, {
            'data': res[counter],
            'fileName': this.resultItemsList.find(e => e.id === id).name
          }];
          counter = counter + 1;

        }
        this.excelService.exportAsExcelFile(reportArray);
      }, err => {
        this.loading = false;
      });
    }
  }

  select(item) {
    this.selectedItem = item;
  }

  selectResultItem(item) {
    this.selectedResultItem = item;
  }

  moveToResult() {
    if (this.selectedItem) {
      this.resultItemsList = [...this.resultItemsList, this.selectedItem];
      this.itemsList = this.itemsList.filter(e => e.id !== this.selectedItem.id);
      this.selectedResultItem = this.selectedItem;
      this.selectedItem = this.itemsList[0];
    }
  }

  moveAllToResult() {
    this.resultItemsList = [...this.resultItemsList, ...this.itemsList];
    this.itemsList = [];
    this.selectedResultItem = this.resultItemsList[0];
    this.selectedItem = null;
  }

  deleteFromResult() {
    if (this.selectedResultItem) {
      this.itemsList = [...this.itemsList, this.selectedResultItem];
      this.resultItemsList = this.resultItemsList.filter(e => e.id !== this.selectedResultItem.id);
      this.selectedItem = this.selectedResultItem;
      this.selectedResultItem = this.resultItemsList[0];
    }
  }

  deleteAllFromResult() {
    this.itemsList = [...this.itemsList, ...this.resultItemsList];
    this.resultItemsList = [];
    this.selectedItem = this.itemsList[0];
    this.selectedResultItem = null;
  }
}
