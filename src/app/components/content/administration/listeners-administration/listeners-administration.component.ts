import {Component, OnInit} from '@angular/core';
import {ListenersService} from '../../../../services/listeners.service';
import {Listener} from '../../../../models/listener';
import {User} from '../../../../models/user';
import {UserService} from '../../../../services/user.service';
import {Subdivision} from '../../../../models/subdivision';
import {forkJoin} from 'rxjs';
import {SubdivisionService} from '../../../../services/subdivisions.service';

@Component({
  selector: 'app-listeners-administration',
  templateUrl: './listeners-administration.component.html',
  styleUrls: ['./listeners-administration.component.css']
})
export class ListenersAdministrationComponent implements OnInit {

  listeners: Listener[];
  hasError = false;
  isFetching = false;
  selectedListener: Listener;
  openDialog = false;
  operators: User[];
  subdivisions: Subdivision[];
  dialogType = 'update';

  constructor(private listenersService: ListenersService,
              private subdivisionService: SubdivisionService,
              private usersService: UserService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.isFetching = true;
    forkJoin(this.listenersService.getAllListeners(),
      this.usersService.getUsersByPrivilege('3'),
      this.subdivisionService.getAllSubdivisions()).subscribe(
      (res) => {
        this.hasError = false;
        this.isFetching = false;
        this.listeners = res[0];
        this.operators = res[1];
        this.subdivisions = res[2];
      }, err => {
        this.isFetching = false;
        this.hasError = true;
      });
  }

  selectListener(listener) {
    this.openDialog = true;
    this.dialogType = 'update';
    this.selectedListener = listener;
  }

  createListener() {
    this.dialogType = 'create';
    this.openDialog = true;
  }

  changeListener() {
    this.getData();
  }

  deleteListener(id) {
    this.isFetching = true;
    this.listenersService.deleteListener(id).subscribe(
      () => {
        this.hasError = false;
        this.isFetching = false;
        this.listeners = this.listeners.filter(e => {
          return e.id !== id;
        });
      },
      () => {
        this.hasError = true;
        this.isFetching = false;
      }
    );
  }
}

