import {Component, OnInit} from '@angular/core';
import {TasksService} from '../../../services/tasks.service';
import {Task} from '../../../models/task';
import {User} from '../../../models/user';
import {UserService} from '../../../services/user.service';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-perform-tasks-table',
  templateUrl: './perform-tasks-table.component.html',
  styleUrls: ['./perform-tasks-table.component.css'],
  animations: [
    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]
})
export class PerformTasksTableComponent implements OnInit {

  tasks: Task[];
  error = false;
  isFetching;
  sizePerPage = 10;
  page = 1;
  totalPages = 1;

  constructor(private taskService: TasksService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    const parameters = {
      page: this.page - 1,
      size: this.sizePerPage,
    };
    this.isFetching = true;
    this.taskService.getTasksForExecutor(parameters).subscribe(res => {
        this.tasks = res.content;
        this.totalPages = res.totalPages;
        this.isFetching = false;
        this.error = false;
      },
      error => {
        this.error = true;
        this.isFetching = false;
      }
    );
  }
}
