import {Component, Input, OnInit} from '@angular/core';
import {TasksService} from '../../../../services/tasks.service';
import {Task} from '../../../../models/task';


@Component({
  selector: 'app-perform-tasks-table-item',
  templateUrl: './perform-tasks-table-item.component.html',
  styleUrls: ['./perform-tasks-table-item.component.css']
})
export class PerformTasksTableItemComponent implements OnInit {

  @Input() task: Task;

  constructor(private taskService: TasksService) {
  }

  ngOnInit() {
  }

  performTask() {
    this.taskService.patchPerformTask(this.task.id).subscribe(() => {
      this.task = null;
    });
  }
}
