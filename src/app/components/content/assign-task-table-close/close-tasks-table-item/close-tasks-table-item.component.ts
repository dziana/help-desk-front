import {Component, Input, OnInit} from '@angular/core';
import {TasksService} from '../../../../services/tasks.service';
import {Task} from '../../../../models/task';


@Component({
  selector: 'app-close-tasks-table-item',
  templateUrl: './close-tasks-table-item.component.html',
  styleUrls: ['./close-tasks-table-item.component.css']
})
export class CloseTasksTableItemComponent implements OnInit {

  @Input() task: Task;

  constructor(private taskService: TasksService) {
  }

  ngOnInit() {
  }

  closeTask() {
    this.taskService.patchStatus(this.task.id, '4').subscribe(() => {
      this.task = null;
    });
  }
}
