import {Component, OnInit} from '@angular/core';
import {TasksService} from '../../../services/tasks.service';
import {Task} from '../../../models/task';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-assign-tasks-table-close',
  templateUrl: './assign-tasks-table-close.component.html',
  styleUrls: ['./assign-tasks-table-close.component.css'],
})
export class AssignTasksTableCloseComponent implements OnInit {
  tasksToClose: Task[];
  error = false;
  isFetching = false;
  sizePerPage = 10;
  page = 1;
  totalPages = 1;

  constructor(private taskService: TasksService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.isFetching = true;
    const parameters = {
      page: this.page - 1,
      size: this.sizePerPage,
      status: '3'
    };
    this.taskService.getTasksByStatus(parameters).subscribe(res => {
        this.error = false;
        this.isFetching = false;
        this.tasksToClose = res.content;
        this.totalPages = res.totalPages;
      },
      error => {
        this.error = true;
        this.isFetching = false;
      }
    );
  }

  changePage(newPage) {
    this.page = newPage;
    this.getData();
  }

}
