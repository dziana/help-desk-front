import {Component, OnInit} from '@angular/core';
import {TasksService} from '../../../services/tasks.service';
import {Task} from '../../../models/task';
import {User} from '../../../models/user';
import {UserService} from '../../../services/user.service';
import {forkJoin} from 'rxjs';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-assign-tasks-table',
  templateUrl: './assign-tasks-table.component.html',
  styleUrls: ['./assign-tasks-table.component.css'],
})
export class AssignTasksTableComponent implements OnInit {

  tasks: Task[];
  executors: User[];
  error = false;
  isFetching;
  sizePerPage = 10;
  page = 1;
  totalPages = 1;

  constructor(private taskService: TasksService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.getFirstData();
  }

  getData() {
    this.isFetching = true;
    const parameters = {
      page: this.page - 1,
      size: this.sizePerPage,
      status: '1'
    };
    this.taskService.getTasksByStatus(parameters).subscribe(res => {
        this.error = false;
        this.isFetching = false;
        this.tasks = res.content;
        this.totalPages = res.totalPages;
      },
      error => {
        this.error = true;
        this.isFetching = false;
      }
    );
  }

  getFirstData() {
    this.isFetching = true;
    const parameters = {
      page: this.page - 1,
      size: this.sizePerPage,
      status: '1'
    };
    forkJoin(this.taskService.getTasksByStatus(parameters)
      , this.userService.getExecutorsBySubdivision())
      .subscribe(res => {
          this.error = false;
          this.isFetching = false;
          this.executors = res[1];
          this.tasks = res[0].content;
          this.totalPages = res[0].totalPages;
        },
        err => {
          this.error = true;
          this.isFetching = false;
        });

  }

  changePage(newPage) {
    this.page = newPage;
    this.getData();
  }
}
