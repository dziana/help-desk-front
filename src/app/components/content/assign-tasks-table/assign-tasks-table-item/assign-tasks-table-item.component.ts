import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../../models/user';
import {TasksService} from '../../../../services/tasks.service';
import {UserService} from '../../../../services/user.service';
import {Task} from '../../../../models/task';


@Component({
  selector: 'app-assign-tasks-table-item',
  templateUrl: './assign-tasks-table-item.component.html',
  styleUrls: ['./assign-tasks-table-item.component.css']
})
export class AssignTasksTableItemComponent implements OnInit {

  @Input() task: Task;
  @Input() executors: User[];
  selectedExecutor;
  dueDate: string;

  constructor(private taskService: TasksService) {
  }

  ngOnInit() {
  }

  setExecutor() {
    if (this.dueDate && this.selectedExecutor) {
      this.taskService.patchAssignTask(this.task.id, new Date(this.dueDate).toISOString(), this.selectedExecutor).subscribe(res => {
        console.log(res);
        this.task = null;
      }, err => {
        console.log(err);
      });
    }
  }
}
