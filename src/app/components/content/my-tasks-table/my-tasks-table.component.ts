import {Component, OnInit} from '@angular/core';
import {TasksService} from '../../../services/tasks.service';
import {Task} from '../../../models/task';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../store/reducers';
import {AddElementsPerPage, AddOne, DeleteAll, Fetch} from '../../../store/actions/action';

@Component({
  selector: 'app-my-tasks-table',
  templateUrl: './my-tasks-table.component.html',
  styleUrls: ['./my-tasks-table.component.css']
})
export class MyTasksTableComponent implements OnInit {

  tasks: Observable<Task[]>;
  hasError: Observable<boolean>;
  isFetching: Observable<boolean>;
  totalPagesObservable: Observable<number>;
  sizePerPage = 10;
  page = 1;
  totalPages = 1;

  constructor(private taskService: TasksService,
              private store: Store<fromRoot.State>) {
    this.tasks = store.select(fromRoot.getTasks);
    this.hasError = store.select(fromRoot.getError);
    this.isFetching = store.select(fromRoot.getFetch);
    this.totalPagesObservable = store.select(fromRoot.getTotal);
  }

  ngOnInit() {
    this.store.dispatch(new AddElementsPerPage(this.sizePerPage));
    this.getData();
  }

  changePage(newPage) {
    this.page = newPage;
    this.getData();
  }

  changeElements() {
    this.store.dispatch(new AddElementsPerPage(this.sizePerPage));
    this.getData();
  }

  getData() {
    this.store.dispatch(new DeleteAll());
    const pagination = {
      page: this.page - 1,
      size: this.sizePerPage
    };
    this.store.dispatch(new Fetch(pagination));
    this.totalPagesObservable.subscribe(e => {
      this.totalPages = e;
    });
  }

  saveTask(task) {
    this.store.dispatch(new AddOne(
      task
    ));
  }

  deleteTask(id: number) {
    this.taskService.deleteTask(id).subscribe(() => {
      this.getData();
    });
  }
}
