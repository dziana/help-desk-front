import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {AuthenticationService} from '../services/authentification.service';


@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.authenticationService.currentUserValue;
    if (localStorage.getItem('currentUser') && currentUser) {
      if (route.data.roles) {
        let allowedRole = false;
        currentUser.user_details.forEach((e) => {
          if (route.data.roles.includes(e['authority'])) {
            allowedRole = true;
          }
        });
        if (allowedRole) {
          return true;
        }
        this.router.navigate(['/auth/login']);
        return false;
      }
      return true;
    }

    this.authenticationService.logout();
    this.router.navigate(['/auth/login']);
    return false;
  }
}
