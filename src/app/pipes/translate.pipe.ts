import { Pipe, PipeTransform } from '@angular/core';
import {translation} from '../../locale/locale-en';


@Pipe({ name: 'translation' })
export class TranslationPipe implements PipeTransform {
  constructor() {}
  transform(value: string): string {
    return translation[value] || null;
  }
}
