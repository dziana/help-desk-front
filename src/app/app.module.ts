import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AdminContainerComponent} from './containers/admin/admin-container.component';
import {MyTasksContainerComponent} from './containers/my-tasks/my-tasks-container.component';
import {AuthContainerComponent} from './containers/auth/auth-container.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {routing} from './app.routing';
import {JwtInterceptor} from './interceptors/jwt.interceptor';
import {ErrorInterceptor} from './interceptors/error.interceptor';
import {LoginFormComponent} from './components/forms/login-form/login-form.component';
import {SignupFormComponent} from './components/forms/signup-form/signup-form.component';
import {AuthenticationService} from './services/authentification.service';
import {MenuComponent} from './components/common/menu/menu.component';
import {AssignTasksContainerComponent} from './containers/assign-tasks/assign-tasks-container.component';
import {PerformTasksContainerComponent} from './containers/perform-tasks/perform-tasks-container.component';
import {CreateTaskFormComponent} from './components/forms/create-task-form/create-task-form.component';
import {TasksService} from './services/tasks.service';
import {MyTasksTableComponent} from './components/content/my-tasks-table/my-tasks-table.component';
import {StoreModule} from '@ngrx/store';
import {reducers} from './store/reducers';
import {EffectsModule} from '@ngrx/effects';
import {LoadService} from './store/effects/load.service';
import {NgxPaginationModule} from 'ngx-pagination';
import {AssignTasksTableComponent} from './components/content/assign-tasks-table/assign-tasks-table.component';
import {AssignTasksTableItemComponent} from './components/content/assign-tasks-table/assign-tasks-table-item/assign-tasks-table-item.component';
import {PerformTasksTableComponent} from './components/content/perform-tasks-table/perform-tasks-table.component';
import {PerformTasksTableItemComponent} from './components/content/perform-tasks-table/perform-tasks-table-item/perform-tasks-table-item.component';
import {CloseTasksTableItemComponent} from './components/content/assign-task-table-close/close-tasks-table-item/close-tasks-table-item.component';
import {BranchesAdministrationComponent} from './components/content/administration/branches-administration/branches-administration.component';
import {MenuAdministrationComponent} from './components/content/administration/menu-administration/menu-administration.component';
import {PrivilegesAdministrationComponent} from './components/content/administration/privileges-administration/privileges-administration.component';
import {SubdivisionAdministrationComponent} from './components/content/administration/subdivision-administration/subdivision-administration.component';
import {UserAdministrationComponent} from './components/content/administration/user-administration/user-administration.component';
import {UpdateUserFormComponent} from './components/forms/update-user-form/update-user-form.component';
import {SubdivisionService} from './services/subdivisions.service';
import {BranchesService} from './services/branches.service';
import {PrivilegeService} from './services/privilege.service';
import {PrivilegesAdministrationItemComponent} from './components/content/administration/privileges-administration/privileges-administration-item/privileges-administration-item.component';
import {UpdateBranchFormComponent} from './components/forms/update-branch-form/update-branch-form.component';
import {UpdateListenerFormComponent} from './components/forms/update-listener-form/update-listener-form.component';
import {UpdateSubdivisionFormComponent} from './components/forms/update-subdivision-form/update-subdivision-form.component';
import {ListenersService} from './services/listeners.service';
import {ListenersAdministrationComponent} from './components/content/administration/listeners-administration/listeners-administration.component';
import {HeaderComponent} from './components/common/header/header.component';
import {ButtonWithSpinnerComponent} from './components/common/button-with-spinner/button-with-spinner.component';
import {SpinnerForButtonComponent} from './components/common/spinner-for-button/spinner-for-button.component';
import {ChangeThemeComponent} from './components/common/header/change-theme/change-theme.component';
import {UserComponent} from './components/common/header/user/user.component';
import {ChangeLanguageComponent} from './components/common/header/change-language/change-language.component';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {AssignTasksTableCloseComponent} from './components/content/assign-task-table-close/assign-tasks-table-close.component';
import {SpinnerComponent} from './components/common/spinner/spinner.component';
import {AssignMenuComponent} from './components/content/assign-menu/assign-menu.component';
import {ExcelService} from './services/excel.service';
import {ReportAdministrationComponent} from './components/content/administration/report-administration/report-administration.component';
import {TranslationPipe} from './pipes/translate.pipe';

@NgModule({
  declarations: [
    AppComponent,
    AdminContainerComponent,
    MyTasksContainerComponent,
    AuthContainerComponent,
    SignupFormComponent,
    CreateTaskFormComponent,
    LoginFormComponent,
    MenuComponent,
    AssignTasksContainerComponent,
    PerformTasksContainerComponent,
    MyTasksTableComponent,
    AssignTasksTableComponent,
    AssignTasksTableCloseComponent,
    AssignTasksTableItemComponent,
    PerformTasksTableComponent,
    PerformTasksTableItemComponent,
    CloseTasksTableItemComponent,
    BranchesAdministrationComponent,
    MenuAdministrationComponent,
    PrivilegesAdministrationComponent,
    PrivilegesAdministrationItemComponent,
    SubdivisionAdministrationComponent,
    UserAdministrationComponent,
    ListenersAdministrationComponent,
    UpdateUserFormComponent,
    UpdateBranchFormComponent,
    UpdateListenerFormComponent,
    UpdateSubdivisionFormComponent,
    HeaderComponent,
    ButtonWithSpinnerComponent,
    SpinnerForButtonComponent,
    ChangeThemeComponent,
    UserComponent,
    ChangeLanguageComponent,
    SpinnerComponent,
    AssignMenuComponent,
    ReportAdministrationComponent,
    TranslationPipe
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    routing,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([LoadService]),
    NgxPaginationModule,
    FormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    CommonModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    AuthenticationService,
    TasksService,
    SubdivisionService,
    BranchesService,
    PrivilegeService,
    ListenersService,
    ExcelService,
    {provide: LocationStrategy, useClass: PathLocationStrategy},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
