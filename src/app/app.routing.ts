import {Routes, RouterModule, PreloadAllModules} from '@angular/router';
import {AuthGuard} from './guards/auth.guard';
import {MyTasksContainerComponent} from './containers/my-tasks/my-tasks-container.component';
import {AdminContainerComponent} from './containers/admin/admin-container.component';
import {Role} from './models/role';
import {AuthContainerComponent} from './containers/auth/auth-container.component';
import {SignupFormComponent} from './components/forms/signup-form/signup-form.component';
import {LoginFormComponent} from './components/forms/login-form/login-form.component';
import {AssignTasksContainerComponent} from './containers/assign-tasks/assign-tasks-container.component';
import {PerformTasksContainerComponent} from './containers/perform-tasks/perform-tasks-container.component';
import {UserAdministrationComponent} from './components/content/administration/user-administration/user-administration.component';
import {SubdivisionAdministrationComponent} from './components/content/administration/subdivision-administration/subdivision-administration.component';
import {BranchesAdministrationComponent} from './components/content/administration/branches-administration/branches-administration.component';
import {PrivilegesAdministrationComponent} from './components/content/administration/privileges-administration/privileges-administration.component';
import {ListenersAdministrationComponent} from './components/content/administration/listeners-administration/listeners-administration.component';
import {AssignTasksTableComponent} from './components/content/assign-tasks-table/assign-tasks-table.component';
import {AssignTasksTableCloseComponent} from './components/content/assign-task-table-close/assign-tasks-table-close.component';
import {ReportAdministrationComponent} from './components/content/administration/report-administration/report-administration.component';

const appRoutes: Routes = [
  {
    path: 'mytasks',
    component: MyTasksContainerComponent,
    canActivate: [AuthGuard],
    data: {roles: [Role.User]}
  },
  {
    path: 'processtasks',
    component: AssignTasksContainerComponent,
    canActivate: [AuthGuard],
    data: {roles: [Role.Operator]},
    children: [
      {
        path: '',
        redirectTo: 'assigntasks',
        pathMatch: 'full'
      },
      {
        path: 'assigntasks',
        component: AssignTasksTableComponent
      },
      {
        path: 'closetasks',
        component: AssignTasksTableCloseComponent
      }
    ]
  },
  {
    path: 'performtasks',
    component: PerformTasksContainerComponent,
    canActivate: [AuthGuard],
    data: {roles: [Role.Executor]}
  },
  {
    path: 'admin',
    component: AdminContainerComponent,
    canActivate: [AuthGuard],
    data: {roles: [Role.Admin]},
    children: [
      {
        path: '',
        redirectTo: 'users',
        pathMatch: 'full'
      },
      {
        path: 'users',
        component: UserAdministrationComponent
      },
      {
        path: 'subdivisions',
        component: SubdivisionAdministrationComponent
      },
      {
        path: 'branches',
        component: BranchesAdministrationComponent
      },
      {
        path: 'listeners',
        component: ListenersAdministrationComponent
      },
      {
        path: 'privileges',
        component: PrivilegesAdministrationComponent
      },
      {
        path: 'report',
        component: ReportAdministrationComponent
      }
    ]
  },
  {
    path: 'auth',
    component: AuthContainerComponent,
    children: [
      {
        path: 'login',
        component: LoginFormComponent
      },
      {
        path: 'signup',
        component: SignupFormComponent
      }
    ]
  },
  {path: '**', redirectTo: 'auth/login'}
];

export const routing = RouterModule.forRoot(appRoutes, {
  preloadingStrategy: PreloadAllModules,
  useHash: true
});
