import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Listener} from '../models/listener';
import {ListenerReport} from '../models/listenerReport';

@Injectable({providedIn: 'root'})
export class ListenersService {
  constructor(private http: HttpClient) {
  }

  getAllListeners() {
    return this.http.get<Listener[]>(`http://localhost:8090/api/listeners`);
  }

  getAllListenersForReport() {
    return this.http.get<ListenerReport[]>(`http://localhost:8090/api/listeners/report`);
  }

  updateListener(id, body) {
    return this.http.patch(`http://localhost:8090/api/listeners/${id}`, body);
  }

  saveListener(body) {
    return this.http.post(`http://localhost:8090/api/listeners`, body);
  }

  deleteListener(id) {
    return this.http.delete(`http://localhost:8090/api/listeners/${id}`);
  }
}
