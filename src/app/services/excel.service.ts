import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import {Sheet} from '../models/sheet';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class ExcelService {

  constructor() { }

  public exportAsExcelFile(sheets: Sheet[]): void {
    let sheetObj = {};
    sheets.forEach( e => {
      sheetObj[e.fileName] = XLSX.utils.json_to_sheet(e.data);
    });
    console.log(sheetObj);
    const sheetNames = sheets.map(({ fileName }) => fileName);
    console.log(sheetNames);
    const workbook: XLSX.WorkBook = { Sheets: sheetObj, SheetNames: sheetNames };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    // const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, 'excelReport');

    // const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet([{'a' : 5, 'b': '6'}, {'a' : 7, 'b': '6'}]);
    // const worksheet1: XLSX.WorkSheet = XLSX.utils.json_to_sheet([{'c' : 5, 'b': '6'}, {'c' : 7, 'b': '6'}]);
    // const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet, 'data1': worksheet1 }, SheetNames: ['data', 'data1'] };
    // const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    // //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    // this.saveAsExcelFile(excelBuffer, 'new');
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

}
