import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Token} from '../models/token';
import {AuthenticationService} from './authentification.service';
import {Task} from '../models/task';
import {TaskPage} from '../models/taskPage';
import {TaskReport} from '../models/taskReport';

@Injectable({providedIn: 'root'})
export class TasksService {
  currentUser: Token;

  constructor(private http: HttpClient,
              private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  createTask(form) {
    const body = {
      name: form.title,
      listenerId: form.listenerId,
      text: form.description,
      systemUserId: this.currentUser.user_id,
      executorId: null,
      operatorId: null,
      statusId: 1,
    };
    return this.http.post<Task>(`http://localhost:8090/api/tasks`, body);
  }

  getPersonalTasks(payload) {
    const headers = {
      params: payload
    };
    return this.http.get<TaskPage>(`http://localhost:8090/api/tasks/personal`, headers);
  }

  deleteTask(id) {
    return this.http.delete<any>(`http://localhost:8090/api/tasks/${id}`);
  }

  getTasksByStatus(payload) {
    const headers = {
      params: payload
    };
    return this.http.get<TaskPage>(`http://localhost:8090/api/tasks/status`, headers);
  }

  patchAssignTask(taskId, dueDate, executor) {
    const body = {
      executorId: executor,
      dueDate: dueDate
    };
    return this.http.patch(`http://localhost:8090/api/tasks/${taskId}/assign`, body);
  }

  patchPerformTask(taskId) {
    return this.http.patch(`http://localhost:8090/api/tasks/${taskId}/execute`, {});
  }

  getTasksForExecutor(payload) {
    const headers = {
      params: payload
    };
    return this.http.get<TaskPage>(`http://localhost:8090/api/tasks/perform`, headers);
  }

  patchStatus(taskId, status) {
    const body = {
      id: status
    };
    return this.http.patch(`http://localhost:8090/api/tasks/${taskId}/status`, body);
  }

  getAllTasksForReport() {
    return this.http.get<TaskReport[]>(`http://localhost:8090/api/tasks/report`);
  }
}
