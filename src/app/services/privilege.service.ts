import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Privilege} from '../models/privilege';

@Injectable({providedIn: 'root'})
export class PrivilegeService {
  constructor(private http: HttpClient) {
  }

  getAllPriviliges() {
    return this.http.get<Privilege[]>(`http://localhost:8090/api/privileges`);
  }
}
