import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subdivision} from '../models/subdivision';

@Injectable({providedIn: 'root'})
export class SubdivisionService {
  constructor(private http: HttpClient) {
  }

  getAllSubdivisions() {
    return this.http.get<Subdivision[]>(`http://localhost:8090/api/subdivisions`);
  }

  deleteSubdivision(id) {
    return this.http.delete(`http://localhost:8090/api/subdivisions/${id}`);
  }

  updateSubdivision(id, body) {
    return this.http.patch(`http://localhost:8090/api/subdivisions/${id}`, body);
  }

  saveSubdivision(body) {
    return this.http.post(`http://localhost:8090/api/subdivisions`, body);
  }
}
