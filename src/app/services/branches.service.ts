import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Branch} from '../models/branch';

@Injectable({providedIn: 'root'})
export class BranchesService {
  constructor(private http: HttpClient) {
  }

  getAllBranches() {
    return this.http.get<Branch[]>(`http://localhost:8090/api/branches`);
  }

  updateBranch(id, body) {
    return this.http.patch(`http://localhost:8090/api/branches/${id}`, body);
  }

  saveBranch(body) {
    return this.http.post(`http://localhost:8090/api/branches`, body);
  }

  deleteBranch(id) {
    return this.http.delete(`http://localhost:8090/api/branches/${id}`);
  }

}
