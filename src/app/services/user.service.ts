import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/user';
import {UserPage} from '../models/userPage';
import {UserReport} from '../models/userReport';

@Injectable({providedIn: 'root'})
export class UserService {
  constructor(private http: HttpClient) {
  }

  getUsersByPrivilege(priv: string) {
    const headers = {
      params: {
        privilege: priv
      }
    };
    return this.http.get<User[]>(`http://localhost:8090/api/users/privilege`, headers);
  }

  getAllUsersForReport() {
    return this.http.get<UserReport[]>(`http://localhost:8090/api/users/report`);
  }

  getAllUsers(payload) {
    const headers = {
      params: payload
    };
    return this.http.get<UserPage>(`http://localhost:8090/api/users`, headers);
  }

  deleteUser(id) {
    return this.http.delete(`http://localhost:8090/api/users/${id}`);
  }

  updateUser(id, body) {
    return this.http.patch(`http://localhost:8090/api/users/${id}`, body);
  }

  updateUserPrivileges(id, privilegeList) {
    const body = {
      privilegeList: privilegeList
    };
    return this.http.patch(`http://localhost:8090/api/users/${id}/privileges`, body);
  }

  getExecutorsBySubdivision() {
    return this.http.get<User[]>(`http://localhost:8090/api/users/subdivision`);
  }
}
