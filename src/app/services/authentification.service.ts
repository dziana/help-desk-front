import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import * as jwt_decode from 'jwt-decode';
import {Token} from '../models/token';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<Token>;
  public currentUser: Observable<Token>;

  constructor(private http: HttpClient) {
    let token;
    if (localStorage.getItem('currentUser')) {
      token = jwt_decode(JSON.parse(localStorage.getItem('currentUser')).token);
    }
    this.currentUserSubject = new BehaviorSubject<Token>(token);
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): Token {
    return this.currentUserSubject.value;
  }

  login(body) {
    return this.http.post<any>(`http://localhost:8090/api/auth/signin`, body)
      .pipe(map(data => {
        if (data && data.token) {
          localStorage.setItem('currentUser', JSON.stringify(data));
          this.currentUserSubject.next(jwt_decode(data.token));
          this.currentUser = this.currentUserSubject.asObservable();
        }
        return data;
      }));
  }

  signup(body) {
    return this.http.post<any>(`http://localhost:8090/api/auth/signup`, body);
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public hasRole(role: string) {
    let flag = false;
    if (this.currentUserSubject.value) {
      for (const user of this.currentUserSubject.value.user_details) {
        if (user['authority'] === role) {
          flag = true;
        }
      }
    }
    return flag;
  }
}
