import {Branch} from './branch';
import {Privilege} from './privilege';
import {Subdivision} from './subdivision';

export class User {
  id: number;
  name: string;
  family: string;
  email: string;
  password: string;
  branchId: Branch;
  privilege: Privilege[];
  subdivisionId: Subdivision;
}
