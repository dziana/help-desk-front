import {Listener} from './listener';
import {ShortUser} from './shortUser';
import {Status} from './status';

export class Task {
  id: number;
  name: string;
  listenerId: Listener;
  text: string;
  systemUserId: ShortUser;
  executorId: ShortUser;
  operatorId: ShortUser;
  statusId: Status;
  createdDate: string;
  executedDate: string;
  dueDate: string;
}
