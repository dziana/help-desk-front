export class TaskSend {
  name: string;
  listenerId: number;
  text: string;
  systemUserId: number;
  executorId: number;
  operatorId: number;
  statusId: number;

  constructor(name?: string,
              listenerId?: number,
              text?: string,
              systemUserId?: number,
              executorId?: number,
              operatorId?: number,
              statusId?: number) {
    this.name = name;
    this.listenerId = listenerId;
    this.text = text;
    this.systemUserId = systemUserId;
    this.executorId = executorId;
    this.operatorId = operatorId;
    this.statusId = statusId;
  }
}
