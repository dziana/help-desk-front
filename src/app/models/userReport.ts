export class UserReport {
  id: number;
  name: string;
  family: string;
  email: string;
  branchId: number;
  privilege: String[];
  subdivisionId: number;
}
