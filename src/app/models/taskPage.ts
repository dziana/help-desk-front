import {Task} from './task';

export class TaskPage {
  content: Task[];
  totalPages: number;
}
