export enum Role {
  User = 'ROLE_USER',
  Admin = 'ROLE_ADMIN',
  Executor = 'ROLE_EXECUTOR',
  Operator = 'ROLE_OPERATOR'
}
