import {User} from './user';
import {Subdivision} from './subdivision';

export class Listener {
  id: number;
  subdivisionId: Subdivision;
  systemUserId: User;
}
