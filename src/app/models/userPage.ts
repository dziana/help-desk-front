import {User} from './user';

export class UserPage {
  content: User[];
  totalPages: number;
}
