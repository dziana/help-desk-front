export class TaskReport {
  id: number;
  name: string;
  listenerId: number;
  text: string;
  systemUserId: number;
  executorId: number;
  operatorId: number;
  statusId: string;
}
