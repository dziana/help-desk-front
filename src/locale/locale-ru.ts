export const translation: any = {
  users: 'Пользователи',
  subdivisions: 'Подразделения',
  listeners: 'Слушатели',
  branches: 'Отделы',
  tasks: 'Задачи',
  admin: 'Администратор',
  user: 'Пользователь',
  executor: 'Исполнитель',
  operator: 'Оператор'
};
