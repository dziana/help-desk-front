export const translation: any = {
  users: 'Users',
  subdivisions: 'Subdivisions',
  listeners: 'Listeners',
  branches: 'Branches',
  tasks: 'Tasks',
  admin: 'Admin',
  user: 'User',
  executor: 'Executor',
  operator: 'Operator'
};
